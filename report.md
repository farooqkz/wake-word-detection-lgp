### Number of coefficients

We did a series of experiments, for 20, 40, 60, 80, 100, 120 and 140 to find the optimal number of coefficients when there
is a limit of 512 instructions and Tournment selection. Welch's T test show that the lower numbers of 20/40/60 perform better.

We hypothese that there should be a reasonable number of coefficients with respect to maximum number of instructions. We also hypothese that
the higher number of coefficients will lead to better accuracy for the positive(or wake word) class. Though we haven't got evidence, yet.

The question is that, does bigger number of coefficients, lead to better accuracy?

### Lack of diversity

The problem we are facing is probably with lack of diversity. To combat this issue, we have various methods. One of which is the demetic approach.
However, this doesn't help much, or doesn't help enough. We could also employ Lexicase selection. But as wake word detection with treated as a binary
classification problem, is an extremely unbalanced one. Lexicase selection leads to far better accuracy on the majority class, usually the negative class and poor accuracy on positive class, the minority class.

We introduce a new method of "Upsampled Lexicase" selection. During Lexicase Upsampled selection, the total number of samples in the minority class is upsampled to make it equal to the number of the majority class. Then, the minority samples appear more than once in the selection phase.

Obviously, when the overall fitness is computed, the minority class accuracy will get higher sooner and easier.

There are other methods to keep diversity intact, two other methods which I know of, are the age based methods and population injection.

The age based methods haven't been tested yet. However, with a handful of experiments on population injection, it seems to have negative effect on the fitness, while further increasing computation cost. However, note that, to make a strong argument, I believe much more experiments need to be done. Currently, only a handful of experiments are done.

### Parsimony pressure

I haven't experimented much on this. I just put a cap of `512` instructions. However, I tried `ThirdAndThird` method. In this method, we group the individuals into 3 buckets by fitness and size. Then the third of most long individuals will get removed if they aren't among the third most fit.

However, this doesn't control or prevent bloat, except for the first 10 generations.

Instead of `ThirdAndThird`, `HalfAndHalf` was also used but without positive effect.

### Demetic parameters

There are multiple variables here:
 - Population size of each deme
 - Number of demes
 - Total population size
 - Migration frequency
 - Migration size

Given a constant computation budget, which is total population size, size of each deme and number of demes are variable. Some experiments has been done with a constant overall population size and trying to change number of demes and population size of each deme. But experiments are not enough, yet.

### How does it learn?

It might be possible that given there are no Jump or Halt in the middle instructions, WakeGP is learning the patterns in each class. And when the negative class is composed of tens of words, it cannot learn the patterns and fails to have good accuracy on the negative class.

To learn if it is really the case, we should compare performance for three kinds of dataset:

 1. Positive a specific word. Negative a specific word. With or without Select Instructions.
 2. Positive a specific word. Negative a set of 2-4 words. With Select instructions.
 3. Positive a single word. Negative anything else. Including noise, music, and a large number of other words.

If it achieves good accuracy for 1 and 2 and bad accuracy for 3, we hypothese that WakeGP is trying to learn patterns on all classes. And we need a new instruction, such as conditional Halt in the middle of execution or conditional forward jumps, so that the solutions will quit in the middle, returning false(therefore a member of the negative class), if a specific pattern is not found.
