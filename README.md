# Hot word detection using Linear Genetic Programming

This program aims to implement hot word detection using Linear Genetic Programming. It also happens to be the program used in this paper published in X.

## Crates used

 - `rand` is used as an RNG
 - `rayon` is used to use multiple threads and thus higher performance
 - `clap` is used to parse command line arguments. You can specify the population and other hyper parameters

## Known issues

 - It seems that on some computers, `rayon` detect 2 processors rather than 4. There are actually 2 processors but the CPU supports 4 threads. This technology is known as hyper threading. To overcome this, you might want to specify number of processors used manually in the code.

## Since this software is written in Rust, is it idiomatic?

I've tried but I'm not a Rust expert, yet.



## License

The GPLv3 or later license. Copyright (C) 2023-2024 Farooq Karimi Zadeh
