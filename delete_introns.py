from pprint import pprint



Instruction = tuple[str, int] | tuple[str, int, int]

instructions: list[Instruction] = list()

with open("/var/tmp/good_model.txt") as fp:
    for ins in  [
        x.strip(",").strip()
        for x in fp.read().split("\n")
    ]:
        ins = ins.replace("(", ",").replace(")", ",")
        ins = ins.split(",")
        if len(ins) == 1:
            continue
        ins_name, arg0, arg1, *_ = ins
        arg0 = int(arg0)
        if arg1 != "":
            arg1 = int(arg1)
            instructions.append((ins_name, arg0, arg1))
        else:
            instructions.append((ins_name, arg0))

for i in range(10):
    print("Pass", i, flush=True)
    targets = [True, False, False, False]
    ptr = len(instructions) - 1
    deleted = False
    while ptr >= 0:
        if not targets[instructions[ptr][1]]:
            del instructions[ptr]
            deleted = True
            old_ptr = ptr
            while abs(old_ptr - ptr) <= 12:
                ptr -= 1
                if instructions[ptr][0].lower() == "jump":
                    if abs(old_ptr - ptr) <= instructions[ptr][2]:
                        new = [
                            instructions[ptr][0],
                            instructions[ptr][1],
                            instructions[ptr][2] - 1
                        ]
                        instructions[ptr] = new
            ptr = old_ptr - 1
        else:
            old_ptr = ptr
            ptr -= 1
            if instructions[old_ptr][0].lower().endswith("m"):
                continue
            if instructions[old_ptr][0].lower() == "load":
                continue
            if instructions[old_ptr][0].lower().startswith("jump"):
                continue
            if len(instructions[old_ptr]) == 2:
                continue
            targets[instructions[old_ptr][2]] = True

pprint(instructions)
