use std::fmt::{Debug, Display, Formatter};
use std::iter::Sum;
use std::ops::{Add, Div};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, PartialOrd, Copy, Clone)]
pub struct Accuracy {
    pub positive: f64,
    pub negative: f64,
}

pub const ZERO_ACCURACY: Accuracy = Accuracy {
    positive: 0.0,
    negative: 0.0,
};

impl Accuracy {
    pub fn max(&self, other: Self) -> Self {
        if other > *self {
            other
        } else {
            *self
        }
    }

    pub fn min(&self, other: Self) -> Self {
        if other < *self {
            other
        } else {
            *self
        }
    }
}

impl Add<Accuracy> for Accuracy {
    type Output = Accuracy;
    fn add(self, other: Self) -> Self {
        Accuracy {
            positive: self.positive + other.positive,
            negative: self.negative + other.negative,
        }
    }
}

impl Div<f64> for Accuracy {
    type Output = Accuracy;
    fn div(self, other: f64) -> Self {
        Accuracy {
            positive: self.positive / other,
            negative: self.negative / other,
        }
    }
}

impl Sum<Self> for Accuracy {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        iter.reduce(|a, b| a + b).unwrap_or(Accuracy {
            positive: 0.0,
            negative: 0.0,
        })
    }
}

impl Display for Accuracy {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "-/+ {:.4}/{:.4}", self.negative, self.positive)
    }
}

impl Debug for Accuracy {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "-/+ {}/{}", self.negative, self.positive)
    }
}
