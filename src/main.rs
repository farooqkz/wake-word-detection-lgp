pub mod accuracy;
pub mod compiler;
pub mod config_file;
pub mod dataset;
pub mod demetic;
pub mod fitness;
pub mod genetic;
pub mod individual;
pub mod instruction;
pub mod mfcc;
pub mod run_result;
pub mod types;

use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

use clap::{arg, command, value_parser};
use config::{Config, File};
use rand::prelude::*;

use crate::{
    config_file::{ConfigFile, FitnessConfig},
    dataset::Dataset,
    demetic::Demetic,
};

fn main() {
    let matches = command!()
        .arg(
            arg!(-c --config <PATH> "The toml config file")
                .required(true)
                .value_parser(value_parser!(String)),
        )
        .arg(
            arg!(-n --name <NAME> "Experiment name")
                .required(false)
                .value_parser(value_parser!(String)),
        )
        .get_matches();
    let mut rng = rand::thread_rng();
    let config_file_path = matches.get_one::<String>("config").unwrap();
    let experiment_no = (rng.next_u32() % 10000).to_string();
    let empty_string = String::new();
    let experiment_name = matches.get_one::<String>("name").unwrap_or(&empty_string);
    println!("Experiment No: {experiment_no}");
    if !experiment_name.is_empty() {
        println!("Experiment name: {experiment_name}");
    }
    let config: ConfigFile = Config::builder()
        .add_source(File::with_name(config_file_path))
        .build()
        .unwrap()
        .try_deserialize::<ConfigFile>()
        .unwrap();
    println!("{}", config);
    rayon::ThreadPoolBuilder::new()
        .num_threads(config.threads)
        .build_global()
        .unwrap();
    let did_receive_interrupt = Arc::new(AtomicBool::new(false));
    let r = did_receive_interrupt.clone();
    let result = ctrlc::set_handler(move || {
        r.store(true, Ordering::SeqCst);
    });
    if result.is_err() {
        eprintln!("Couldn't listen to CtrlC");
    }

    let Ok(mut train_dataset) = Dataset::new(&config.dataset_config) else {
        panic!("Cannot read dataset");
    };
    let test_dataset = train_dataset.split_test(&config.dataset_config);
    println!("Constructing initial population");
    let mut _1trainer = Demetic::new(&config.first_stage, config.fitness_fn);
    println!("Constructed initial population");
    _1trainer.run(true, &train_dataset, did_receive_interrupt.clone());
    let fitness_config = FitnessConfig {
        max_program_size: None,
        compile: config.first_stage.fitness_config.compile,
        clean: None,
    };
    let mut best = _1trainer
        .get_most_fit(Some(&train_dataset.sub(0.0, 1.0)), Some(&fitness_config))
        .clone();
    best.clean();
    println!("Stack len: {}", best.len().0);
    best.compute_fitness(&fitness_config, train_dataset.sub(0.0, 1.0));
    println!("Train accuracy: {}", best.get_accuracy());
    best.compute_fitness(&fitness_config, test_dataset.sub(0.0, 1.0));
    println!("Test accuracy: {}", best.get_accuracy());
    println!("{best}");
    if let Some(second_stage_config) = config.second_stage.as_ref() {
        println!("Now second stage trainer");
        let mut _2trainer = Demetic::from_individual(second_stage_config, config.fitness_fn, &best);
        _2trainer.run(true, &train_dataset, did_receive_interrupt.clone());
    }
}
