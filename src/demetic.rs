use std::{
    fs::write,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::Duration,
};

use rayon::prelude::*;

use crate::dataset::{Dataset, SubDataset};
use crate::fitness::FitnessFn;
use crate::genetic::Genetic;
use crate::individual::Individual;
use crate::run_result::{RunResult, RunResultItem};
use crate::types::FitnessVal;
use crate::{
    config_file::{FitnessConfig, TrainerConfig},
    types::Plot,
};

pub struct Demetic {
    pub demes: Vec<Genetic>,
    props: TrainerConfig,
    fitness_fn: FitnessFn,
}

fn duration_pprint(d: Duration) -> String {
    let mut seconds: u64 = d.as_secs();
    let hours: u64 = seconds / 3600;
    seconds -= hours * 3600;
    let minutes: u64 = seconds / 60;
    seconds -= minutes * 60;
    format!("[{:0>3}:{:0>2}:{:0>2}]", hours, minutes, seconds)
}

impl Demetic {
    pub fn from_individual(props: &TrainerConfig, fitness_fn: FitnessFn, ind: &Individual) -> Self {
        let mut trainer = Self {
            demes: vec![],
            props: props.clone(),
            fitness_fn,
        };
        for _ in 0..props.number_of_demes {
            let mut gen = Genetic::from_individual(props.clone(), fitness_fn, ind);
            gen.regenerate_constants();
            trainer.demes.push(gen);
        }
        trainer
    }

    pub fn new(props: &TrainerConfig, fitness_fn: FitnessFn) -> Self {
        let mut trainer = Self {
            demes: vec![],
            props: props.clone(),
            fitness_fn,
        };
        for _ in 0..props.number_of_demes {
            trainer.demes.push(Genetic::new(props.clone(), fitness_fn));
        }
        trainer
    }
    /*
    pub fn scale_up(&mut self, new_population: usize) {
        self.demes.par_iter_mut().for_each(|deme| {
            deme.scale_up(new_population);
        })
    }
    */
    /*
    fn pre_train(&mut self, log: bool, dataset: &Dataset) {
        if let Some(pre_train_config) = self.props.pre_train_config {
            if log {
                println!("Starting pre training...");
            }
            let dataset_fraction_per_deme: f32 = pre_train_config.dataset_fraction / self.self.demes.len() as f32;
            let sub_datasets: Vec<SubDataset> = (0..self.demes.len()))
                .map(|i|
                    dataset.sub(
                        i as f32 * dataset_fraction_per_deme,
                        dataset_fraction_per_deme
                    )
                )
                .collect();
            let end_condition = pre_training_config.end_condition;
            let mut gen: usize = 1;
            let start_time = std::time::Instant::now();
            loop {
                let mut run_results: Vec<RunResultItem> = self
                    .demes
                    .par_iter_mut()
                    .enumerate()
                    .map(|(i, deme)| {
                        deme.run_for(1, &sub_datasets[i]).last()
                    })
                    .collect();
                let mut avg_accuracy = run_results
                    .iter()
                    .reduce(|acc, e| {
                        acc.0[1] + e.0[1]
                    })
                    .unwrap();
                avg_accuracy /= self.demes.len() as f64;
                if let Some(max_gen) = end_condition.generations {
                    if gen >= max_gen {
                        break;
                    }
                }
                if let Some(max_accuracy) = end_condition.accuracy {
                    if avg_accuracy >= max_accuracy {
                        break;
                    }
                }
                if let Some(max_minutes) = end_condition.minutes {
                    if start_time.elapsed().as_secs() / 60 >= max_minutes {
                        break;
                    }
                }
                gen += 1;
            }
        } else {
            if log {
                println!("No pre training");
            }
        }

    }
    */
    pub fn run(
        &mut self,
        log: bool,
        dataset: &Dataset,
        did_receive_interrupt: Arc<AtomicBool>,
    ) -> bool {
        let mut run_result: RunResult = RunResult::new();
        let mut g: usize = 1;
        let mut plot: Plot = if let Some(generations) = self.props.end_condition.generations {
            Vec::with_capacity(generations)
        } else {
            Vec::new()
        };
        let start_time = std::time::Instant::now();
        let migration_population =
            (self.props.migration_rate.unwrap_or(0.0) * self.props.population as f32) as usize;
        let end_condition = &self.props.end_condition;
        loop {
            let mut run_results_and_bests: Vec<(RunResultItem, Vec<Individual>)> = self
                .demes
                .par_iter_mut()
                .enumerate()
                .map(|(_i, deme)| {
                    let run_result = deme.run_for(1, dataset.sub(0.0, 1.0));
                    let top_n = deme.get_n_most_fit(migration_population);
                    (run_result.last(), top_n)
                })
                .collect();
            if self.demes.len() > 1 && g % self.props.migration_freq.unwrap_or(usize::MAX) == 0 {
                run_results_and_bests.rotate_right(1);
                run_results_and_bests
                    .par_iter_mut()
                    .zip(self.demes.par_iter_mut())
                    .for_each(|((_, bests), deme)| {
                        deme.replace_n_worst(bests);
                    });
            }
            let (mut avg_accuracy, mut avg_fitness, mut avg_size) = run_results_and_bests
                .iter()
                .map(|(result, _)| *result)
                .reduce(|acc, e| (acc.0 + e.0, acc.1 + e.1, acc.2 + e.2))
                .unwrap();
            let len = self.demes.len() as f64;
            avg_accuracy = avg_accuracy / len;
            avg_fitness /= len;
            avg_size /= len;
            run_result.push(avg_accuracy, avg_fitness, avg_size);
            if log {
                println!(
                    "{} Gen {g} avg/avg_ft/ft_div/avg_size {} {:.4} {:.4} {:.1}",
                    duration_pprint(start_time.elapsed()),
                    avg_accuracy,
                    avg_fitness,
                    self.get_avg_diversity(),
                    avg_size,
                );
            }
            plot.push(self.get_fitness_values());
            g += 1;
            if let Some(max_generations) = end_condition.generations {
                if g > max_generations {
                    break;
                }
            }
            if let Some(max_accuracy) = end_condition.accuracy {
                if avg_accuracy >= max_accuracy {
                    break;
                }
            }
            if let Some(max_minutes) = end_condition.minutes {
                if start_time.elapsed().as_secs() / 60 >= max_minutes {
                    break;
                }
            }
            if let Some(diversity) = end_condition.diversity {
                if self.get_avg_diversity() <= diversity {
                    break;
                }
            }
            if did_receive_interrupt.load(Ordering::SeqCst) {
                did_receive_interrupt.store(false, Ordering::SeqCst);
                break;
            }
        }
        if let Some(plot_file_path) = self.props.plot_file_path.as_ref() {
            let mut s = String::new();
            for generation in plot.iter() {
                for val in generation.iter() {
                    s.push_str(format!("{val:.3},").as_str())
                }
                s.push('\n');
            }
            if let Err(e) = write(plot_file_path.clone(), s) {
                println!("Cannot write to {plot_file_path}: {e:?}");
            } else {
                println!("Successfully wrote to {plot_file_path}");
            }
        }
        true
    }

    pub fn get_most_fit(
        &mut self,
        dataset: Option<&SubDataset>,
        fitness_config: Option<&FitnessConfig>,
    ) -> &Individual {
        if let Some(dataset) = dataset {
            self.demes.iter_mut().for_each(|deme| {
                deme.recompute_ft_values(*dataset, fitness_config);
            });
        }
        let elites: Vec<&Individual> = self.demes.iter().map(|deme| deme.get_most_fit()).collect();
        let mut max = elites[0];
        for ind in elites.iter() {
            if ind.ft.calc_fitness(&self.fitness_fn) > max.ft.calc_fitness(&self.fitness_fn) {
                max = ind;
            }
        }
        max
    }

    fn get_fitness_values(&self) -> Vec<FitnessVal> {
        let mut result = Vec::with_capacity(self.demes.len() * self.demes[0].population.len());
        for deme in self.demes.iter() {
            let mut v = deme.get_fitness_values();
            result.append(&mut v);
        }
        result
    }

    pub fn get_least_complex(&self) -> &Individual {
        self.demes
            .iter()
            .map(|deme| deme.get_least_complex())
            .min_by_key(|ind| ind.len())
            .unwrap()
    }

    pub fn get_avg_diversity(&self) -> f64 {
        self.demes
            .iter()
            .map(|gen| gen.get_fitness_diversity())
            .sum::<f64>()
            / self.demes.len() as f64
    }
}
