use std::{cmp::Ordering, collections::BTreeSet, slice::Iter};

use rand::prelude::*;
use rayon::{iter::repeatn, prelude::*};
use serde::{Deserialize, Serialize};

use crate::{
    accuracy::Accuracy,
    config_file::{FitnessConfig, TrainerConfig},
    dataset::SubDataset,
    fitness::FitnessFn,
    individual::Individual,
    instruction::InstructionGenerator,
    run_result::RunResult,
    types::FitnessVal,
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SelectionMethod {
    Tournment(usize),
    Lexicase(f64),
    Random,
    Truncate,
    Average,
}

pub struct Genetic {
    pub population: Vec<Individual>,
    pub props: TrainerConfig,
    fitness_fn: FitnessFn,
    generator: InstructionGenerator,
    is_population_sorted: bool,
}

impl Genetic {
    pub fn from_individual(props: TrainerConfig, fitness_fn: FitnessFn, ind: &Individual) -> Self {
        Self {
            generator: InstructionGenerator::default(),
            is_population_sorted: false,
            fitness_fn,
            population: repeatn(ind.clone(), props.population).collect(),
            props,
        }
    }

    pub fn regenerate_constants(&mut self) {
        self.population
            .par_iter_mut()
            .for_each(|ind| ind.regenerate_constants());
    }

    pub fn new(props: TrainerConfig, fitness_fn: FitnessFn) -> Self {
        let generator = InstructionGenerator::new(&props.program_generation.instruction_gen);
        let cap: usize = {
            let cap =
                (1.0 + props.crossover_rate + props.reproduction_rate) * props.population as f32;
            cap as usize + props.population_injection.unwrap_or(0)
        };

        let mut population: Vec<Individual> = Vec::with_capacity(cap);
        let mut init_population: Vec<Individual> = (0..props.population)
            .into_par_iter()
            .map(|_| Individual::new(&props, &generator))
            .collect();
        population.append(&mut init_population);
        Genetic {
            population,
            fitness_fn,
            props,
            is_population_sorted: false,
            generator,
        }
    }

    pub fn scale_up(&mut self, new_population: usize) {
        if new_population <= self.population.len() {
            return;
        }
        if (new_population - self.population.len()) % 2 != 0 {
            panic!("Not modulo 2!");
        }
        self.crossover(new_population);
    }

    pub fn recompute_ft_values(&mut self, dataset: SubDataset, props: Option<&FitnessConfig>) {
        self.population.par_iter_mut().for_each(|ind| {
            ind.compute_fitness(props.unwrap_or(&self.props.fitness_config), dataset)
        });
        self.is_population_sorted = false;
    }

    pub fn run_for(&mut self, generations: usize, dataset: SubDataset) -> RunResult {
        let pop = self.props.population as f32;
        let mut rng = rand::thread_rng();
        let mut run_result: RunResult = RunResult::new();
        let crossover_pop = (self.props.crossover_rate * pop) as usize;
        let reproduction_pop = (self.props.reproduction_rate * pop) as usize;
        for _g in 0..generations {
            self.recompute_ft_values(dataset, None);
            self.is_population_sorted = false;
            self.population
                .iter()
                .enumerate()
                .map(|(idx, ind)| (idx, ind.ft.calc_fitness(&self.fitness_fn)))
                .collect::<Vec<(usize, f64)>>()[..]
                .choose_multiple_weighted(&mut rng, reproduction_pop, |(_idx, ft)| *ft)
                .unwrap()
                .map(|(idx, _ind)| idx)
                .for_each(|idx| self.population.push(self.population[*idx].clone()));
            self.recompute_ft_values(dataset, None);
            self.crossover(crossover_pop);
            self.population.par_iter_mut().for_each(|ind| {
                if rand::thread_rng().gen_bool(self.props.micro_mutation_rate) {
                    (0..self.props.changes_per_micro).for_each(|_| {
                        ind.mutate_micro(&self.generator);
                    });
                }
            });
            self.population.par_iter_mut().for_each(|ind| {
                if rand::thread_rng().gen_bool(self.props.macro_mutation_rate) {
                    (0..self.props.changes_per_macro).for_each(|_| {
                        ind.mutate_macro(&self.generator);
                    });
                }
            });
            self.population.par_iter_mut().for_each(|ind| {
                if rand::thread_rng().gen_bool(self.props.macro_constant_mutation_rate) {
                    ind.mutate_constant_macro();
                }
            });
            self.population.par_iter_mut().for_each(|ind| {
                if rand::thread_rng().gen_bool(self.props.micro_constant_mutation_rate) {
                    ind.mutate_constant_micro();
                }
            });
            if let Some(pop_inject) = self.props.population_injection {
                (0..pop_inject).for_each(|_| {
                    self.population
                        .push(Individual::new(&self.props, &self.generator));
                });
            }
            self.recompute_ft_values(dataset, None);
            self.selection();
            self.increase_age();
            run_result.push(
                self.get_total_accuracy() / pop as f64,
                self.get_avg_fitness(),
                self.get_avg_size(),
            );
        }
        run_result
    }

    fn crossover(&mut self, crossover_pop: usize) {
        let mut rng = rand::thread_rng();
        let parents: Vec<&Individual> = self.population[..]
            .choose_multiple_weighted(&mut rng, crossover_pop, |ind| {
                ind.ft.calc_fitness(&self.fitness_fn)
            })
            .unwrap()
            .collect();

        let mut offsprings: Vec<Individual> = parents
            .par_iter()
            .zip(parents.par_iter().rev())
            .flat_map(|(father, mother)| {
                let (off0, off1) = mother.crossover(father, self.props.crossover_prop);
                vec![off0, off1]
            })
            .collect();
        self.population.append(&mut offsprings);
    }

    fn tournment_select(&self, size: usize) -> usize {
        let mut rng = rand::thread_rng();
        let selected: Vec<(usize, f64)> = self
            .population
            .iter()
            .map(|ind| (ind.id, ind.ft.calc_fitness(&self.fitness_fn)))
            .choose_multiple(&mut rng, size);
        // max_id is thee index of the winner, max_ft is the fitness of the winner
        let (max_id, _max_ft) = selected
            .iter()
            .max_by_key(|(_id, ft)| (ft * 100000.0) as usize)
            .unwrap();
        *max_id
    }

    fn selection(&mut self) {
        use SelectionMethod::*;
        let mut rng = rand::thread_rng();
        self.population
            .retain(|ind| ind.ft.calc_fitness(&self.fitness_fn) > 0.0);
        match self.props.selection {
            Tournment(size) => {
                let mut new_population: BTreeSet<usize> = BTreeSet::new();
                new_population.par_extend(
                    repeatn(true, self.props.population)
                        .into_par_iter()
                        .map(|_i| self.tournment_select(size)),
                );
                while new_population.len() < self.props.population {
                    new_population.insert(self.tournment_select(size));
                }
                self.population
                    .retain(|ind| new_population.contains(&ind.id));
                assert_eq!(self.population.len(), self.props.population);
            }
            Random => {
                while self.population.len() > self.props.population {
                    let idx = rng.next_u64() as usize % self.population.len();
                    self.population.swap_remove(idx);
                }
            }
            Average => {
                while self.population.len() > self.props.population {
                    let avg = self
                        .population
                        .iter()
                        .map(|ind| ind.ft.calc_fitness(&self.fitness_fn))
                        .sum::<f64>()
                        / self.population.len() as f64;
                    let idx = rng.next_u64() as usize % self.population.len();
                    if self.population[idx].ft.calc_fitness(&self.fitness_fn) < avg {
                        self.population.swap_remove(idx);
                    }
                }
            }
            Truncate => {
                self.sort_by_fitness();
                self.population.truncate(self.props.population);
            }
            Lexicase(size) => {
                let partition_no = (1.0 / size).ceil() as usize;
                let mut partitions: Vec<(f64, f64)> = (0..partition_no)
                    .map(|i| {
                        let start = i as f64 * size;
                        let end = (start + size).min(1.0);
                        (start, end)
                    })
                    .collect();

                let mut new_population: BTreeSet<_> = BTreeSet::new();
                while new_population.len() != self.props.population {
                    partitions.shuffle(&mut rng);
                    let mut specialists: Vec<(usize, &Individual)> =
                        Vec::with_capacity(self.population.len());
                    for part in partitions.iter() {
                        match specialists.len() {
                            0 => {
                                let population: Vec<(usize, &Individual)> =
                                    self.population.iter().enumerate().collect();
                                specialists.clone_from(&get_specialists_for_partition(
                                    *part,
                                    population.iter(),
                                    &self.fitness_fn,
                                ));
                            }
                            1 => {
                                break;
                            }
                            _ => {
                                let mut new_specilists = get_specialists_for_partition(
                                    *part,
                                    specialists.iter(),
                                    &self.fitness_fn,
                                );
                                specialists.clear();
                                specialists.append(&mut new_specilists);
                            }
                        }
                    }
                    new_population.insert(specialists[0].0);
                }
                let mut current_index: usize = 0;
                self.population.retain(|_ind| {
                    let b = new_population.contains(&current_index);
                    current_index += 1;
                    b
                });
            }
        }
    }

    fn increase_age(&mut self) {
        self.population.par_iter_mut().for_each(|ind| {
            ind.age += 1;
        });
    }

    pub fn get_fitness_values(&self) -> Vec<FitnessVal> {
        self.population
            .iter()
            .map(|ind| ind.ft.calc_fitness(&self.fitness_fn))
            .collect()
    }

    pub fn replace_n_worst(&mut self, inds: &mut Vec<Individual>) {
        self.sort_by_fitness();
        self.population.truncate(self.population.len() - inds.len());
        self.population.append(inds);
    }

    fn get_total_accuracy(&self) -> Accuracy {
        self.population
            .par_iter()
            .map(|ind| ind.get_accuracy())
            .sum::<Accuracy>()
    }

    pub fn get_avg_size(&self) -> f64 {
        self.population
            .iter()
            .map(|ind| ind.len().0 as f64)
            .sum::<f64>()
            / self.population.len() as f64
    }

    fn get_total_fitness(&self) -> f64 {
        self.population
            .par_iter()
            .map(|ind| ind.ft.calc_fitness(&self.fitness_fn))
            .sum::<f64>()
    }

    fn get_avg_fitness(&self) -> f64 {
        self.get_total_fitness() / self.props.population as f64
    }

    pub fn get_most_fit(&self) -> &Individual {
        if self.is_population_sorted {
            return self.population.last().unwrap();
        }
        let mut max = &self.population[0];
        for ind in self.population.iter() {
            if ind.ft.calc_fitness(&self.fitness_fn) > max.ft.calc_fitness(&self.fitness_fn) {
                max = ind;
            }
        }
        max
    }

    pub fn get_n_most_fit(&mut self, n: usize) -> Vec<Individual> {
        if !self.is_population_sorted {
            self.sort_by_fitness();
        }
        self.population[..n].to_vec()
    }

    pub fn get_least_fit(&self) -> &Individual {
        self.population
            .par_iter()
            .min_by(|ind_a, ind_b| {
                ind_a
                    .ft
                    .calc_fitness(&self.fitness_fn)
                    .partial_cmp(&ind_b.ft.calc_fitness(&self.fitness_fn))
                    .unwrap_or(Ordering::Equal)
            })
            .unwrap()
    }

    pub fn get_least_complex(&self) -> &Individual {
        self.population
            .par_iter()
            .min_by_key(|ind| ind.len())
            .unwrap()
    }
    /*
    pub fn dump_population(&self, path: &Path) -> std::io::Result<()> {
        let population_dump: Vec<u8> = self
            .population
            .par_iter()
            .map(|ind| {
                let mut dump = ind.dump();
                for _ in 0..8 {
                    dump.push(0u8);
                } // separator
                dump
            })
            .flatten()
            .collect();
        let mut fd = File::open(path)?;
        fd.write(&population_dump[..])?;
        Ok(())
    }
    */

    fn sort_by_fitness(&mut self) {
        self.population.par_sort_unstable_by(|ind1, ind2| {
            ind2.ft
                .calc_fitness(&self.fitness_fn)
                .partial_cmp(&ind1.ft.calc_fitness(&self.fitness_fn))
                .unwrap_or(Ordering::Equal)
        });
    }

    pub fn get_fitness_diversity(&self) -> f64 {
        let mut fitnesses: Vec<f64> = self
            .population
            .iter()
            .map(|ind| ind.ft.calc_fitness(&self.fitness_fn))
            .collect();
        fitnesses.par_sort_unstable_by(|ft1, ft2| ft2.partial_cmp(ft1).unwrap_or(Ordering::Equal));
        let mean = fitnesses[fitnesses.len() / 2];
        let mut diversity: f64 = 0.0;
        self.population.iter().for_each(|ind| {
            diversity += (mean - ind.ft.calc_fitness(&self.fitness_fn)).abs();
        });
        diversity / self.population.len() as f64
    }

    pub fn get_behavior_diversity(&mut self) -> f64 {
        self.sort_by_fitness();
        let mut diversity: f64 = 0.0;
        for (ind_a, ind_b) in self.population.iter().zip(self.population.iter().skip(1)) {
            diversity += ind_a.difference(ind_b);
        }
        diversity
    }
}

fn get_specialists_for_partition<'a>(
    (start, end): (f64, f64),
    elements: Iter<(usize, &'a Individual)>,
    fitness_fn: &FitnessFn,
) -> Vec<(usize, &'a Individual)> {
    elements.fold(vec![], move |mut acc, el| {
        if acc.is_empty() {
            acc.push(*el);
        } else {
            let current_max: f64 = acc[0].1.ft.calc_lexicase_fitness(start, end, fitness_fn);
            let element_ft: f64 = el.1.ft.calc_lexicase_fitness(start, end, fitness_fn);
            match current_max
                .partial_cmp(&element_ft)
                .unwrap_or(Ordering::Equal)
            {
                Ordering::Equal => {
                    acc.push(*el);
                }
                Ordering::Less => {
                    acc[0] = *el;
                }
                Ordering::Greater => {}
            }
        }
        acc
    })
}
