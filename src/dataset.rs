use std::{
    fs::{read_dir, File},
    io::{Error, ErrorKind::*},
    path::{Path, PathBuf},
};

use cached::proc_macro::io_cached;
use rand::prelude::*;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use wav::bit_depth::BitDepth;

use crate::{
    config_file::{DatasetConfig, MfccExtractorConfig},
    mfcc::MfccExtractor,
};

const SIXTEEN_POWER: f32 = (1 << 16) as f32;
const EIGHT_POWER: f32 = (1 << 8) as f32;
const TWENTYFOUR_POWER: f32 = (1 << 24) as f32;
const THIRTYTWO_POWER: f32 = (1i64 << 32i64) as f32;
// Powers of 2 used for normalization in Datapoint::from_file

pub trait HasDataset {
    fn get_positive(&self) -> &[Datapoint];
    fn get_negative(&self) -> &[Datapoint];
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Datapoint {
    pub data: Vec<f32>,
}

#[io_cached(
    disk = true,
    map_error = r##"|e| anyhow::Error::new(e)"##,
    convert = r##"{ format!("{}{config}", path.display()) }"##,
    key = "String"
)]
fn _datapoint_from_file(
    path: &Path,
    config: &MfccExtractorConfig,
    normalize: bool,
) -> anyhow::Result<Datapoint> {
    let mut inp_file;
    inp_file = File::open(path)?;
    let (_, data) = wav::read(&mut inp_file)?;
    use BitDepth::*;
    let data = match data {
        Sixteen(data) => data.iter().map(|x| *x as f32 / SIXTEEN_POWER).collect(),
        Eight(data) => data.iter().map(|x| *x as f32 / EIGHT_POWER).collect(),
        TwentyFour(data) => data.iter().map(|x| *x as f32 / TWENTYFOUR_POWER).collect(),
        ThirtyTwoFloat(data) => data.iter().map(|x| { *x } / THIRTYTWO_POWER).collect(),
        Empty => vec![],
    };
    if data.is_empty() {
        Err(anyhow::Error::new(Error::new(Other, "It's empty.")))
    } else {
        let mut extractor = MfccExtractor::new(config);
        let mfcc = extractor.compute(&data[..]);
        let data = if normalize {
            let (max, min): (f32, f32) = data.iter().fold((-1.0, 10000.0), |acc, el| {
                (
                    if *el < acc.0 { acc.0 } else { *el },
                    if *el > acc.1 { acc.1 } else { *el },
                )
            });
            let diff = (max - min).abs();
            mfcc.iter().flatten().map(|x| x / diff).collect()
        } else {
            mfcc.into_iter().flatten().collect()
        };
        Ok(Datapoint { data })
    }
}

impl Datapoint {
    fn from_file(
        path: &Path,
        config: &MfccExtractorConfig,
        normalize: bool,
    ) -> anyhow::Result<Self> {
        _datapoint_from_file(path, config, normalize)
    }
    /*
    pub fn dump_to_file(&self, path: &Path) -> anyhow::Result<usize> {
        let mut f = File::create(path)?;
        let mut written: usize = 0;
        for val in self.data.iter() {
            let val = val.to_le_bytes();
            written += f.write(&val[..])?;
        }
        Ok(written)
    }
    */
}

pub struct Dataset {
    pub positive: Vec<Datapoint>,
    pub negative: Vec<Datapoint>,
}

impl Dataset {
    pub fn new(config: &DatasetConfig) -> anyhow::Result<Self> {
        println!("Reading files from disk...");

        let mut neg_entries: Vec<PathBuf> = vec![];
        let mut pos_entries: Vec<PathBuf> = vec![];
        for entry in read_dir(Path::new(&config.dataset_neg_path))? {
            let entry = entry?;
            if entry.file_type()?.is_file() {
                neg_entries.push(entry.path());
            }
        }
        for entry in read_dir(Path::new(&config.dataset_pos_path))? {
            let entry = entry?;
            if entry.file_type()?.is_file() {
                pos_entries.push(entry.path());
            }
        }

        let neg_datapoints: Vec<Datapoint> = neg_entries
            .par_iter()
            .map(|path| {
                Datapoint::from_file(
                    path,
                    &config.extractor_config,
                    config.normalize.unwrap_or(false),
                )
            })
            .filter(|r| r.is_ok())
            .map(|r| r.unwrap())
            .collect();
        let pos_datapoints: Vec<Datapoint> = pos_entries
            .par_iter()
            .map(|path| {
                Datapoint::from_file(
                    path,
                    &config.extractor_config,
                    config.normalize.unwrap_or(false),
                )
            })
            .filter(|r| r.is_ok())
            .map(|r| r.unwrap())
            .collect();
        println!(
            "I have {} positive and {} negative samples",
            pos_datapoints.len(),
            neg_datapoints.len()
        );
        Ok(Dataset {
            positive: pos_datapoints,
            negative: neg_datapoints,
        })
    }

    pub fn split_test(&mut self, config: &DatasetConfig) -> Self {
        let mut rng = rand::thread_rng();
        let test_count_pos: usize =
            (self.positive.len() as f32 * config.test_dataset_fraction_pos) as usize;
        let test_count_neg: usize =
            (self.negative.len() as f32 * config.test_dataset_fraction_neg) as usize;
        self.positive.shuffle(&mut rng);
        self.negative.shuffle(&mut rng);
        let test_neg: Vec<_> = self.negative.drain(0..test_count_neg).collect();
        let test_pos: Vec<_> = self.positive.drain(0..test_count_pos).collect();

        Dataset {
            positive: test_pos,
            negative: test_neg,
        }
    }

    pub fn sub(&self, offset: f32, fraction: f32) -> SubDataset {
        let mut rng = rand::thread_rng();
        let positive_count = (self.positive.len() as f32 * fraction) as usize;
        let negative_count = (self.negative.len() as f32 * fraction) as usize;
        let p_start = (self.positive.len() as f32 * offset) as usize;
        let p_end = (p_start + positive_count).min(self.positive.len());
        let n_start = (self.negative.len() as f32 * offset) as usize;
        let n_end = (n_start + negative_count).min(self.negative.len());
        SubDataset {
            positive: &self.positive[p_start..p_end],
            negative: &self.negative[n_start..n_end],
            id: rng.next_u32(),
        }
    }
}

#[derive(Clone, Copy)]
pub struct SubDataset<'a> {
    pub positive: &'a [Datapoint],
    pub negative: &'a [Datapoint],
    pub id: u32,
}
