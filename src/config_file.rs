use crate::{accuracy::Accuracy, fitness::FitnessFn, genetic::SelectionMethod};
use serde::{Deserialize, Serialize};
use std::{
    fmt::{Display, Error, Formatter},
    hash::Hash,
};

#[derive(Deserialize, Clone)]
pub struct ConfigFile {
    pub first_stage: TrainerConfig,
    pub second_stage: Option<TrainerConfig>,
    pub threads: usize,
    pub dataset_config: DatasetConfig,
    pub fitness_fn: FitnessFn,
}

impl Display for ConfigFile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Using fitness function {:?}.", self.fitness_fn)?;
        if self.threads == 0 {
            write!(f, "Using all CPU threads.")?;
        } else {
            write!(f, "Using {} CPU threads.", self.threads)?;
        }
        write!(
            f,
            "{:#?}\n{:#?}\n{:#?}",
            self.dataset_config, self.first_stage, self.second_stage
        )?;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TrainerConfig {
    pub population: usize,
    pub number_of_demes: usize,
    pub crossover_rate: f32,
    pub crossover_prop: CrossOverProp,
    pub macro_mutation_rate: f64,
    pub micro_mutation_rate: f64,
    pub micro_constant_mutation_rate: f64,
    pub macro_constant_mutation_rate: f64,
    pub changes_per_micro: usize,
    pub changes_per_macro: usize,
    pub reproduction_rate: f32,
    pub program_generation: ProgramGenerationConfig,
    pub number_of_constants: u16,
    pub end_condition: EndCondition,
    pub migration_rate: Option<f32>,
    pub migration_freq: Option<usize>,
    pub population_injection: Option<usize>,
    pub selection: SelectionMethod,
    pub bucket_size: Option<usize>,
    pub fitness_config: FitnessConfig,
    pub plot_file_path: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Copy)]
pub struct CrossOverProp {
    pub constant: Option<CrossOverType>,
    pub program: Option<CrossOverType>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Copy)]
pub enum CrossOverType {
    OnePoint,
    TwoPoints,
    FourPoints,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ProgramGenerationConfig {
    pub range_up: usize,
    pub range_down: usize,
    pub instruction_gen: InstructionGenerationConfig,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FitnessConfig {
    pub max_program_size: Option<usize>,
    pub compile: Option<bool>,
    pub clean: Option<bool>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct InstructionGenerationConfig {
    pub register_cells: usize,
    pub memory_cells: usize,
    pub constant_cells: usize,
    pub jump_range: [usize; 2],
    pub instructions: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EndCondition {
    pub minutes: Option<u64>,
    pub generations: Option<usize>,
    pub accuracy: Option<Accuracy>,
    pub diversity: Option<f64>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DatasetConfig {
    pub dataset_neg_path: String,
    pub dataset_pos_path: String,
    pub normalize: Option<bool>,
    pub test_dataset_fraction_pos: f32,
    pub test_dataset_fraction_neg: f32,
    pub extractor_config: MfccExtractorConfig,
}

#[derive(Serialize, Deserialize, Clone, Debug, Hash)]
pub struct MfccExtractorConfig {
    pub sample_rate: usize,
    pub samples_per_frame: usize,
    pub samples_per_shift: usize,
    pub num_coefficients: usize,
}

impl Display for MfccExtractorConfig {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "{}{}{}{}",
            self.sample_rate, self.samples_per_frame, self.samples_per_shift, self.num_coefficients
        )
    }
}
