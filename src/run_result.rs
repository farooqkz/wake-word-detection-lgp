use crate::accuracy::{Accuracy, ZERO_ACCURACY};

pub struct Vec1<T>(Vec<T>);

impl<T> Vec1<T> {
    pub fn last(&self) -> &T {
        self.0.last().unwrap()
    }

    pub fn new(v: Vec<T>) -> Self {
        if v.is_empty() {
            panic!("v must have at least one element");
        }
        Self(v)
    }
}

pub struct RunResult {
    pub avg_accuracy_values: Vec1<Accuracy>,
    pub avg_fitness_values: Vec1<f64>,
    pub avg_sizes: Vec1<f64>,
}

pub type RunResultItem = (Accuracy, f64, f64);

impl Default for RunResult {
    fn default() -> Self {
        Self::new()
    }
}

impl RunResult {
    pub fn new() -> Self {
        let z: Vec<Accuracy> = vec![ZERO_ACCURACY];
        RunResult {
            avg_accuracy_values: Vec1::new(z.clone()),
            avg_fitness_values: Vec1::new(vec![0.0]),
            avg_sizes: Vec1::new(vec![0.0]),
        }
    }

    pub fn push(&mut self, accuracy: Accuracy, avg_fitness: f64, avg_size: f64) {
        self.avg_accuracy_values.0.push(accuracy);
        self.avg_fitness_values.0.push(avg_fitness);
        self.avg_sizes.0.push(avg_size);
    }

    pub fn last(&self) -> RunResultItem {
        (
            *self.avg_accuracy_values.last(),
            *self.avg_fitness_values.last(),
            *self.avg_sizes.last(),
        )
    }
}
