use libm::{cosf, expf, log10f, powf, sinf, sqrtf};
use serde::Deserialize;

use crate::{
    compiler::Program,
    config_file::FitnessConfig,
    dataset::SubDataset,
    instruction::{Instruction, MAXIMUM_REGISTERS, OUTPUT_REGISTER},
};

#[derive(Clone, Hash)]
pub struct FtPreResult {
    pub pos: Vec<bool>,
    pub neg: Vec<bool>,
}

impl FtPreResult {
    pub fn fn_(&self) -> f64 {
        self.neg.iter().filter(|b| !**b).count() as f64
    }
    pub fn tp(&self) -> f64 {
        self.pos.iter().filter(|b| **b).count() as f64
    }
    pub fn tn(&self) -> f64 {
        self.neg.iter().filter(|b| **b).count() as f64
    }
    pub fn fp(&self) -> f64 {
        self.pos.iter().filter(|b| !**b).count() as f64
    }
}

#[derive(Debug, Clone, Deserialize, Copy)]
pub enum FitnessFn {
    Classic,
    Exp,
    DoubleExp,
    TripleExp,
}

#[derive(Clone, Default)]
pub struct Fitness {
    pre: Option<FtPreResult>,
    stack: Vec<Instruction>,
    constants: Vec<f32>,
    dataset_id: Option<u32>,
    compiled_program: Option<Program>,
}

impl Fitness {
    pub fn eval(
        &mut self,
        dataset: SubDataset,
        stack: &Vec<Instruction>,
        constants: &Vec<f32>,
        config: &FitnessConfig,
    ) {
        let need_update = {
            if let Some(dataset_id) = self.dataset_id {
                dataset.id != dataset_id || self.stack != *stack || self.constants != *constants
            } else {
                true
            }
        };
        if !need_update {
            return;
        }
        self.constants.clone_from(constants);
        self.stack.clone_from(stack);
        self.dataset_id = Some(dataset.id);
        if stack.len() > config.max_program_size.unwrap_or(usize::MAX) {
            self.pre = Some(FtPreResult {
                pos: vec![false],
                neg: vec![false],
            });
            return;
        }
        if config.compile.unwrap_or(false) {
            if let Some(mut prog) = Program::new(&stack[..], &constants[..], config.clean) {
                prog.allocate();
                self.compiled_program.replace(prog);
                self.pre.replace(
                    self.compiled_program
                        .as_ref()
                        .unwrap()
                        .execute_dataset(dataset),
                );
                return;
            } else {
                eprintln!("Cannot compile program. Is the host machine architecture supported?");
            }
        }
        let mut pos: Vec<bool> = Vec::with_capacity(dataset.positive.len());
        for datapoint in dataset.positive.iter() {
            let predicted = evaluate(&self.stack, &datapoint.data, &self.constants);
            pos.push(predicted); // it should predict true as it's positive class
        }
        let mut neg: Vec<bool> = Vec::with_capacity(dataset.negative.len());
        for datapoint in dataset.negative.iter() {
            let predicted = evaluate(&self.stack, &datapoint.data, &self.constants);
            neg.push(!predicted); // it should predict false as its negative class
        }
        self.pre = Some(FtPreResult { neg, pos });
    }

    pub fn calc_fitness(&self, fitness_fn: &FitnessFn) -> f64 {
        if let Some(pre) = self.pre.as_ref() {
            let p_acc = pre.tp() / (pre.fp() + pre.tp());
            let n_acc = pre.tn() / (pre.fn_() + pre.tn());
            use FitnessFn::*;
            match fitness_fn {
                Classic => (p_acc * n_acc).sqrt(),
                Exp => (p_acc * n_acc).sqrt().exp(),
                DoubleExp => (p_acc * n_acc).sqrt().exp().exp(),
                TripleExp => (p_acc * n_acc).sqrt().exp().exp().exp(),
            }
        } else {
            -1.0
        }
    }

    pub fn calc_lexicase_fitness(&self, start: f64, end: f64, fitness_fn: &FitnessFn) -> f64 {
        if let Some(pre) = self.pre.as_ref() {
            let p_acc = {
                let start = (start * pre.pos.len() as f64) as usize;
                let end = (end * pre.pos.len() as f64) as usize;
                pre.pos[start..end].iter().filter(|b| **b).count() as f64 / (end - start) as f64
            };
            let n_acc = {
                let start = (start * pre.neg.len() as f64) as usize;
                let end = (end * pre.neg.len() as f64) as usize;
                pre.neg[start..end].iter().filter(|b| **b).count() as f64 / (end - start) as f64
            };
            use FitnessFn::*;
            match fitness_fn {
                Classic => (p_acc * n_acc).sqrt(),
                Exp => (p_acc * n_acc).sqrt().exp(),
                DoubleExp => (p_acc * n_acc).sqrt().exp().exp(),
                TripleExp => (p_acc * n_acc).sqrt().exp().exp().exp(),
            }
        } else {
            -1.0
        }
    }

    pub fn get_pre(&self) -> &FtPreResult {
        self.pre.as_ref().unwrap()
    }

    pub fn is_pre_populated(&self) -> bool {
        self.pre.is_some()
    }
}

pub fn evaluate(stack: &[Instruction], args: &[f32], constants: &[f32]) -> bool {
    let mut registers: [f32; MAXIMUM_REGISTERS] = [0.0; MAXIMUM_REGISTERS];
    let mut pc: usize = 0;
    use crate::instruction::Instruction::*;
    while pc < stack.len() {
        match stack[pc] {
            Sum(r0, r1) => {
                registers[r0] += registers[r1];
            }
            Multiply(r0, r1) => {
                registers[r0] *= registers[r1];
            }
            Load(r0, mem) => {
                registers[r0] = args[mem];
            }
            Sub(r0, r1) => {
                registers[r0] -= registers[r1];
            }
            CopyReg(r0, r1) => {
                registers[r0] = registers[r1];
            }
            JumpZN(r0, rjmp) => {
                if registers[r0] <= 0.0 {
                    pc += rjmp;
                }
            }
            JumpP(r0, rjmp) => {
                if registers[r0] > 0.0 {
                    pc += rjmp;
                }
            }
            SumM(r0, mem) => {
                registers[r0] += args[mem];
            }
            MultiplyM(r0, mem) => {
                registers[r0] *= args[mem];
            }
            SubM(r0, mem) => {
                registers[r0] -= args[mem];
            }
            Cube(r0) => {
                registers[r0] = powf(registers[r0], 3.0);
            }
            Square(r0) => {
                registers[r0] = powf(registers[r0], 2.0);
            }
            Sin(r0) => {
                registers[r0] = sinf(registers[r0]);
            }
            Cos(r0) => {
                registers[r0] = cosf(registers[r0]);
            }
            Log(r0) => {
                registers[r0] = log10f(registers[r0].abs());
            }
            Exp(r0) => {
                registers[r0] = expf(registers[r0]);
            }
            Sqrt(r0) => {
                registers[r0] = sqrtf(registers[r0].abs());
            }
            LoadC(r0, const_) => {
                registers[r0] = constants[const_];
            }
        }
        pc += 1;
    }
    registers[OUTPUT_REGISTER] <= 0.0
}
