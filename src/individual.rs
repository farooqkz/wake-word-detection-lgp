use std::{
    collections::BTreeMap,
    fmt::{Display, Formatter},
    mem::discriminant,
};

use rand::prelude::*;

use crate::{
    accuracy::Accuracy,
    config_file::{CrossOverProp, CrossOverType, FitnessConfig, TrainerConfig},
    dataset::SubDataset,
    fitness::Fitness,
    instruction::{
        clean_introns, flatten_block, unflatten_block, CodeBlock, Instruction, InstructionGenerator,
    },
};

pub struct Individual {
    pub stack: Vec<Instruction>,
    pub age: usize,
    pub ft: Fitness,
    pub id: usize,
    constants: Vec<f32>,
}

impl Clone for Individual {
    fn clone(&self) -> Self {
        Self {
            stack: self.stack.clone(),
            age: 0,
            constants: self.constants.clone(),
            ft: Fitness::default(),
            id: rand::thread_rng().gen::<usize>(),
        }
    }
}

fn generate_const() -> f32 {
    let mut rng = rand::thread_rng();
    (rng.gen::<f32>() - 0.5) * 2.0
}

impl Individual {
    pub fn regenerate_constants(&mut self) {
        self.constants.clone_from(
            &(0..self.constants.len())
                .map(|_i| generate_const())
                .collect(),
        );
    }

    pub fn clean(&mut self) {
        let mut cb = self.codeblock();
        clean_introns(&mut cb);
        self.stack.clone_from(&flatten_block(&cb));
    }

    pub fn delete_constant_introns(&mut self) -> usize {
        let mut constants_used: BTreeMap<usize, usize> = BTreeMap::new();
        let mut idx = 0;
        for ins in self
            .stack
            .iter()
            .filter(|ins| matches!(ins, Instruction::LoadC(_r, _c)))
        {
            match ins {
                Instruction::LoadC(_r, c_idx) => {
                    if !constants_used.contains_key(c_idx) {
                        constants_used.insert(*c_idx, idx);
                        idx += 1;
                    }
                }
                _ => unreachable!(),
            }
        }
        for ins in self
            .stack
            .iter_mut()
            .filter(|ins| matches!(ins, Instruction::LoadC(_r, _c)))
        {
            match ins {
                Instruction::LoadC(r, c_idx) => {
                    if let Some(new_c_idx) = constants_used.get(c_idx) {
                        *ins = Instruction::LoadC(*r, *new_c_idx);
                    }
                }
                _ => unreachable!(),
            }
        }
        self.constants.truncate(idx);
        self.constants.len()
    }

    pub fn difference(&self, other: &Self) -> f64 {
        let self_behavior = self.get_behavior();
        let other_behavior = other.get_behavior();
        let mut result = 0.0f64;
        let mut count = 0.0f64;
        for (i1, i2) in self_behavior.iter().zip(other_behavior.iter()) {
            result += (i1 ^ i2).count_ones() as f64;
            count += 64.0;
        }
        result / count
    }

    pub fn new(config: &TrainerConfig, generator: &InstructionGenerator) -> Self {
        let program_generation = &config.program_generation;
        let mut rng = rand::thread_rng();
        let r = program_generation.range_down..=program_generation.range_up;
        let stack: Vec<Instruction> = (0..rng.gen_range(r))
            .map(|_| generator.generate_one())
            .collect();
        let constants: Vec<f32> = (0..config.number_of_constants)
            .map(|i| i as f32 / 200.0)
            .collect();
        Individual {
            ft: Fitness::default(),
            stack,
            constants,
            age: 0,
            id: rng.gen::<usize>(),
        }
    }

    pub fn len(&self) -> (usize, usize) {
        (self.stack.len(), self.constants.len())
    }

    pub fn crossover(&self, other: &Self, crossover_props: CrossOverProp) -> (Self, Self) {
        let mut rng = rand::thread_rng();
        let (stack0, stack1) = crossover_props
            .program
            .map(|ty| crossover(ty, &self.stack[..], &other.stack[..]))
            .unwrap_or((self.stack.clone(), self.stack.clone()));
        let (const0, const1) = crossover_props
            .constant
            .map(|ty| crossover(ty, &self.constants[..], &other.constants[..]))
            .unwrap_or((self.constants.clone(), self.constants.clone()));
        let new0 = Individual {
            ft: Fitness::default(),
            stack: stack0,
            constants: const0,
            age: 0,
            id: rng.gen::<usize>(),
        };
        let new1 = Individual {
            ft: Fitness::default(),
            stack: stack1,
            constants: const1,
            age: 0,
            id: rng.gen::<usize>(),
        };
        (new0, new1)
    }

    pub fn codeblock(&self) -> Vec<CodeBlock> {
        unflatten_block(&self.stack[..])
    }

    pub fn mutate_constant_macro(&mut self) {
        let mut rng = rand::thread_rng();
        let index = rng.gen_range(0..self.constants.len());
        self.constants[index] = generate_const();
    }

    pub fn mutate_constant_micro(&mut self) {
        let mut rng = rand::thread_rng();
        let index = rng.gen_range(0..self.constants.len());
        self.constants[index] *= (rng.gen::<f32>() - 0.5) * 0.02;
    }

    pub fn mutate_macro(&mut self, generator: &InstructionGenerator) {
        let mut rng = rand::thread_rng();
        let instruction_index = rng.gen_range(0..self.len().0);
        self.stack[instruction_index] = generator.generate_one();
    }

    pub fn mutate_micro(&mut self, generator: &InstructionGenerator) {
        let mut rng = rand::thread_rng();
        let index = rng.gen_range(0..self.len().0);
        match self.stack[index] {
            Instruction::JumpP(_r, dist) => {
                self.stack[index] =
                    Instruction::JumpP(rng.gen::<usize>() % generator.props.register_cells, dist);
            }
            Instruction::JumpZN(_r, dist) => {
                self.stack[index] =
                    Instruction::JumpZN(rng.gen::<usize>() % generator.props.register_cells, dist);
            }
            _ => {
                self.stack[index] = loop {
                    let i = generator.generate_one();
                    if discriminant(&i) == discriminant(&self.stack[index]) {
                        break i;
                    }
                }
            }
        }
    }

    pub fn compute_fitness(&mut self, config: &FitnessConfig, dataset: SubDataset) {
        self.ft.eval(dataset, &self.stack, &self.constants, config);
    }

    pub fn get_behavior(&self) -> Vec<u64> {
        let mut counter: usize = 0;
        let mut current_num: u64 = 0;
        let pre = self.ft.get_pre();
        let mut result: Vec<u64> = Vec::with_capacity((pre.pos.len() + pre.neg.len()) / 62);
        for bit in pre.pos.iter().chain(pre.neg.iter()) {
            if *bit {
                current_num += 1;
            }
            current_num <<= 1;
            counter += 1;
            if counter == 63 {
                result.push(current_num);
                counter = 0;
                current_num = 0;
            }
        }
        result
    }

    pub fn get_accuracy(&self) -> Accuracy {
        let pre = self.ft.get_pre();
        Accuracy {
            positive: (pre.tp() / (pre.tp() + pre.fp())) * 100.0,
            negative: (pre.tn() / (pre.tn() + pre.fn_())) * 100.0,
        }
    }
}

fn crossover<T: Clone>(ty: CrossOverType, self_: &[T], other: &[T]) -> (Vec<T>, Vec<T>) {
    let mut rng = thread_rng();
    use CrossOverType::*;
    match ty {
        OnePoint => {
            let pt = rng.gen_range(0..self_.len().min(other.len()));
            let (stack_right_0, stack_left_0) = self_.split_at(pt);
            let (stack_right_1, stack_left_1) = other.split_at(pt);
            (
                [stack_right_0, stack_left_1].concat(),
                [stack_right_1, stack_left_0].concat(),
            )
        }
        TwoPoints => {
            let (stack_right_0, stack_left_0) = self_.split_at(rng.gen_range(0..self_.len()));
            let (stack_right_1, stack_left_1) = other.split_at(rng.gen_range(0..other.len()));
            (
                [stack_right_0, stack_left_1].concat(),
                [stack_right_1, stack_left_0].concat(),
            )
        }
        FourPoints => {
            let gen_points = |slice: &[T]| {
                let mut rng = rand::thread_rng();
                let mut pts = [0usize; 2];
                pts[0] = rng.gen_range(0..slice.len());
                pts[1] = rng.gen_range(0..slice.len());
                if pts[0] > pts[1] {
                    let t = pts[0];
                    pts[0] = pts[1];
                    pts[1] = t;
                }
                pts[1] -= pts[0];
                pts
            };
            let points_self = gen_points(self_);
            let points_other = gen_points(other);
            let (part_self_right, part_self_middle_left) = self_.split_at(points_self[0]);
            let (part_self_middle, part_self_left) = part_self_middle_left.split_at(points_self[1]);
            let (part_other_right, part_other_middle_left) = other.split_at(points_other[0]);
            let (part_other_middle, part_other_left) =
                part_other_middle_left.split_at(points_other[1]);
            (
                [part_self_right, part_other_middle, part_self_left].concat(),
                [part_other_right, part_self_middle, part_other_left].concat(),
            )
        }
    }
}

impl Display for Individual {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "[")?;
        for ins in self.stack.iter() {
            writeln!(f, "{:?},", ins)?;
        }
        writeln!(f, "]")?;
        write!(f, "{:?}", self.constants)?;
        Ok(())
    }
}
