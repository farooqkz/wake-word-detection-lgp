pub type FitnessVal = f64;

pub type Plot = Vec<Vec<FitnessVal>>;
