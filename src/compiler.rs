use std::mem::transmute;

use libm::{cosf, expf, log10f, sinf};

use cranelift::prelude::*;
use cranelift::{
    codegen::{
        control::ControlPlane,
        ir::{types::F32, AbiParam, Function, InstBuilder, Signature, UserFuncName},
        isa::{lookup, CallConv},
        verifier::verify_function,
        Context,
    },
    frontend::{FunctionBuilder, FunctionBuilderContext},
};
use memmap2::Mmap;
use target_lexicon::Triple;

use crate::{
    dataset::{Datapoint, SubDataset},
    fitness::FtPreResult,
    instruction::{Instruction, MAXIMUM_REGISTERS, OUTPUT_REGISTER},
};

pub struct Program {
    code: Vec<u8>,
    buffer: Option<Mmap>,
}

impl Clone for Program {
    fn clone(&self) -> Self {
        Self {
            code: self.code.clone(),
            buffer: None,
        }
    }
}

const MEM_CELL_SIZE: i32 = 4; // number of bytes in a f32

impl Program {
    pub fn new(stack: &[Instruction], constants: &[f32], _clean: Option<bool>) -> Option<Self> {
        let mut builder = settings::builder();
        if let Err(err) = builder.set("opt_level", "none") {
            eprintln!("Cannot set optimization level to none: {err:?}");
        }
        let flags = settings::Flags::new(builder);
        let isa = match lookup(Triple::host()) {
            Err(_) => return None,
            Ok(isa_builder) => isa_builder.finish(flags).unwrap(),
        };
        let (self_sig, ptr_type) = {
            let pointer_type = isa.pointer_type();
            let mut sig = Signature::new(CallConv::SystemV);
            sig.params.push(AbiParam::new(pointer_type)); // args
            sig.params.push(AbiParam::new(pointer_type)); // registers
            sig.returns.push(AbiParam::new(F32));
            (sig, pointer_type)
        };
        let lib_func_sig = {
            let mut sig2 = Signature::new(CallConv::SystemV);
            sig2.params.push(AbiParam::new(F32));
            sig2.returns.push(AbiParam::new(F32));
            sig2
        };
        let mut fctx = FunctionBuilderContext::new();
        let mut func = Function::with_name_signature(UserFuncName::default(), self_sig);
        let mut func_builder = FunctionBuilder::new(&mut func, &mut fctx);
        let lib_func_sigref = func_builder.import_signature(lib_func_sig);

        let block = func_builder.create_block();
        func_builder.seal_block(block);
        func_builder.append_block_params_for_function_params(block);
        func_builder.switch_to_block(block);
        let args_addr = func_builder.block_params(block)[0];
        let registers_ptr = func_builder.block_params(block)[1];
        let constants: Vec<Value> = constants
            .iter()
            .map(|c| func_builder.ins().f32const(*c))
            .collect();
        let (log10, exp, sin, cos) = {
            let exp = func_builder
                .ins()
                .iconst(ptr_type, expf as *const () as i64);
            let log10 = func_builder
                .ins()
                .iconst(ptr_type, log10f as *const () as i64);
            let sin = func_builder
                .ins()
                .iconst(ptr_type, sinf as *const () as i64);
            let cos = func_builder
                .ins()
                .iconst(ptr_type, cosf as *const () as i64);
            (log10, exp, sin, cos)
        };
        let fp_zero = func_builder.ins().f32const(0.0);
        let fp_minus_one = func_builder.ins().f32const(-1.0);
        use Instruction::*;
        let mem_flags = MemFlags::new();
        for instruction in stack.iter() {
            match instruction {
                Sum(r0, r1) => {
                    let r0_ = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let r1 = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r1 as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fadd(r0_, r1);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Sub(r0, r1) => {
                    let r0_ = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let r1 = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r1 as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fsub(r0_, r1);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Multiply(r0, r1) => {
                    let r0_ = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let r1 = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r1 as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fmul(r0_, r1);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                SumM(r0, mem) => {
                    let r = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let mem = func_builder.ins().load(
                        F32,
                        mem_flags,
                        args_addr,
                        *mem as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fadd(r, mem);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                SubM(r0, mem) => {
                    let r = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let mem = func_builder.ins().load(
                        F32,
                        mem_flags,
                        args_addr,
                        *mem as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fsub(r, mem);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                MultiplyM(r0, mem) => {
                    let r = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let mem = func_builder.ins().load(
                        F32,
                        mem_flags,
                        args_addr,
                        *mem as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fmul(r, mem);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Exp(r0) => {
                    let r_val = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let inst = func_builder
                        .ins()
                        .call_indirect(lib_func_sigref, exp, &[r_val]);
                    let r_val = func_builder.inst_results(inst)[0];
                    func_builder.ins().store(
                        mem_flags,
                        r_val,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Cos(r0) => {
                    let r_val = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let inst = func_builder
                        .ins()
                        .call_indirect(lib_func_sigref, cos, &[r_val]);
                    let r_val = func_builder.inst_results(inst)[0];
                    func_builder.ins().store(
                        mem_flags,
                        r_val,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Sin(r0) => {
                    let r_val = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let inst = func_builder
                        .ins()
                        .call_indirect(lib_func_sigref, sin, &[r_val]);
                    let r_val = func_builder.inst_results(inst)[0];
                    func_builder.ins().store(
                        mem_flags,
                        r_val,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Load(r0, mem) => {
                    let val = func_builder.ins().load(
                        F32,
                        mem_flags,
                        args_addr,
                        *mem as i32 * MEM_CELL_SIZE,
                    );
                    func_builder.ins().store(
                        mem_flags,
                        val,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Sqrt(r0) => {
                    let val = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    // invert the sign if r0 is negative
                    let sign = func_builder.ins().fcmp(FloatCC::GreaterThan, fp_zero, val);
                    // 1 if sgn(r0) == -1 else 0
                    let val_neg = func_builder.ins().fmul(val, fp_minus_one);
                    let val = func_builder.ins().select(sign, val_neg, val);

                    let result = func_builder.ins().sqrt(val);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                CopyReg(r0, r1) => {
                    let val1 = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r1 as i32 * MEM_CELL_SIZE,
                    );
                    func_builder.ins().store(
                        mem_flags,
                        val1,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Square(r0) => {
                    let val0 = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fmul(val0, val0);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Cube(r0) => {
                    let val0 = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    let result = func_builder.ins().fmul(val0, val0);
                    let result = func_builder.ins().fmul(result, val0);
                    func_builder.ins().store(
                        mem_flags,
                        result,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                Log(r0) => {
                    let r_val = func_builder.ins().load(
                        F32,
                        mem_flags,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                    // invert the sign if r0 is negative
                    let sign = func_builder
                        .ins()
                        .fcmp(FloatCC::GreaterThan, fp_zero, r_val);
                    // 1 if sgn(r0) == -1 else 0
                    let r_val_neg = func_builder.ins().fmul(r_val, fp_minus_one);
                    let r_val = func_builder.ins().select(sign, r_val_neg, r_val);
                    let inst = func_builder
                        .ins()
                        .call_indirect(lib_func_sigref, log10, &[r_val]);
                    let r_val = func_builder.inst_results(inst)[0];
                    func_builder.ins().store(
                        mem_flags,
                        r_val,
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                LoadC(r0, c) => {
                    func_builder.ins().store(
                        mem_flags,
                        constants[*c],
                        registers_ptr,
                        *r0 as i32 * MEM_CELL_SIZE,
                    );
                }
                _ => unreachable!(),
            }
        }
        let output = func_builder.ins().load(
            F32,
            mem_flags,
            registers_ptr,
            OUTPUT_REGISTER as i32 * MEM_CELL_SIZE,
        );
        func_builder.ins().return_(&[output]);
        func_builder.finalize();
        if let Err(error) = verify_function(&func, &*isa) {
            eprintln!("Verification error {error:?}");
            return None;
        }
        let mut ctx = Context::for_function(func);
        let mut cplane = ControlPlane::default();
        let code: Vec<u8> = match ctx.compile(&*isa, &mut cplane) {
            Ok(x) => x.code_buffer().to_vec(),
            Err(error) => {
                eprintln!("Compilation error {error:?}");
                return None;
            }
        };
        Some(Self { code, buffer: None })
    }

    pub fn allocate(&mut self) {
        let mut buffer = memmap2::MmapOptions::new()
            .len(self.code.len())
            .map_anon()
            .unwrap();

        buffer.copy_from_slice(self.code.as_slice());

        let buffer = buffer.make_exec().unwrap();
        self.buffer.replace(buffer);
    }

    pub fn execute(&self, args: &Datapoint) -> bool {
        let result = unsafe {
            let mut registers = [0.0f32; MAXIMUM_REGISTERS];
            let func: unsafe extern "sysv64" fn(*const f32, *const f32) -> f32 =
                transmute(self.buffer.as_ref().unwrap().as_ptr());
            func(args.data.as_ptr(), registers.as_mut_ptr())
        };
        result <= 0.0
    }

    pub fn execute_dataset(&self, dataset: SubDataset) -> FtPreResult {
        let func: unsafe extern "sysv64" fn(*const f32, *const f32) -> f32 =
            unsafe { transmute(self.buffer.as_ref().unwrap().as_ptr()) };
        FtPreResult {
            pos: dataset
                .positive
                .iter()
                .map(|datapoint| unsafe {
                    let mut registers = [0.0f32; MAXIMUM_REGISTERS];
                    func(datapoint.data.as_ptr(), registers.as_mut_ptr()) <= 0.0
                })
                .collect(),
            neg: dataset
                .negative
                .iter()
                .map(|datapoint| unsafe {
                    let mut registers = [0.0f32; MAXIMUM_REGISTERS];
                    func(datapoint.data.as_ptr(), registers.as_mut_ptr()) > 0.0
                })
                .collect(),
        }
    }
}
