use std::{
    collections::{BTreeMap, HashSet},
    mem::{discriminant, Discriminant},
    str::FromStr,
};

use rand::prelude::*;
use serde::{Deserialize, Serialize};
use strum_macros::EnumString;

use crate::config_file::InstructionGenerationConfig;

pub const OUTPUT_REGISTER: usize = 0;
pub const MAXIMUM_REGISTERS: usize = 128;

pub type AllowedInstructions = HashSet<Discriminant<Instruction>>;
pub type AllowedInstructionsRaw = Vec<Instruction>;

#[derive(EnumString, Clone, Debug, Copy, PartialEq, Eq, Deserialize, Serialize)]
#[strum(serialize_all = "PascalCase", ascii_case_insensitive)]
pub enum Instruction {
    Sum(usize, usize),       // r0 += r1
    Multiply(usize, usize),  // r0 *= r1
    Load(usize, usize),      // r0 = mem[usize]
    Sub(usize, usize),       // r0 -= r0
    CopyReg(usize, usize),   // r0 = r1
    JumpZN(usize, usize),    // skip the next r1 instruction(s) if r0 < 0.0
    MultiplyM(usize, usize), // r0 *= mem[usize]
    SumM(usize, usize),      // r0 += mem[usize]
    SubM(usize, usize),      // r0 -= mem[usize]
    Square(usize),           // r0 *= r0
    Cube(usize),             // r0 *= r0 * r0
    JumpP(usize, usize),     // skip the next r1 instrucion(s) if r0 > 0.0
    Sin(usize),              // r0 = sin(r0)
    Log(usize),              // r0 = log(r0)
    Cos(usize),              // r0 = cos(r0)
    Exp(usize),              // r0 = exp(r0)
    Sqrt(usize),             // r0 = sqrt(r0)
    LoadC(usize, usize),     // r0 = constants[r1]
}

pub struct InstructionGenerator {
    allowed_instructions: AllowedInstructions,
    pub props: InstructionGenerationConfig,
}

impl Default for InstructionGenerator {
    fn default() -> Self {
        use Instruction::*;
        let mut allowed_instructions: AllowedInstructions = HashSet::with_capacity(8);
        let allowed = [
            Load(0, 0),
            LoadC(0, 0),
            Sum(0, 0),
            Multiply(0, 0),
            SumM(0, 0),
            MultiplyM(0, 0),
            Sqrt(0),
        ];
        for i in allowed.iter() {
            allowed_instructions.insert(discriminant(i));
        }
        let props = InstructionGenerationConfig {
            register_cells: 4,
            constant_cells: 4,
            memory_cells: 4,
            jump_range: [0, 0],
            instructions: vec![],
        };
        Self {
            allowed_instructions,
            props,
        }
    }
}

impl InstructionGenerator {
    pub fn new(props: &InstructionGenerationConfig) -> Self {
        let mut allowed_instructions: AllowedInstructions = HashSet::with_capacity(20);
        for i in props.instructions.iter() {
            if let Ok(i) = Instruction::from_str(i) {
                allowed_instructions.insert(discriminant(&i));
            }
        }
        Self {
            props: props.clone(),
            allowed_instructions,
        }
    }

    pub fn generate_many(&self, n: usize) -> Vec<Instruction> {
        let mut result: Vec<Instruction> = vec![];
        let mut jumps: BTreeMap<usize, usize> = BTreeMap::new();
        use Instruction::*;
        let mut current_pos = 0usize;
        while current_pos < n {
            let new_ins = self.generate_one();
            match new_ins {
                JumpZN(_r, dist) | JumpP(_r, dist) => {
                    let jump_positions: Vec<usize> = jumps.keys().cloned().collect();
                    for pos in jump_positions.iter().rev() {
                        if current_pos - pos <= *jumps.get(pos).unwrap() {
                            continue;
                        } else {
                            result.push(new_ins);
                            jumps.insert(current_pos, dist);
                            current_pos += 1;
                        }
                    }
                }
                _ => {
                    result.push(new_ins);
                    current_pos += 1;
                }
            }
        }
        result
    }

    pub fn generate_one(&self) -> Instruction {
        let InstructionGenerationConfig {
            register_cells,
            memory_cells,
            jump_range,
            instructions: _,
            constant_cells,
        } = &self.props;
        let mut rng = rand::thread_rng();
        let r0 = rng.gen_range(0..*register_cells);
        let r1: usize = loop {
            let r = rng.gen_range(0..*register_cells);
            if r != r0 {
                break r;
            }
        };
        let rjmp: usize = rng.gen_range(jump_range[0]..=jump_range[1]);
        let mem: usize = rng.gen_range(0..*memory_cells);
        let c: usize = rng.gen_range(0..*constant_cells);
        use Instruction::*;
        let instructions = [
            Load(r0, mem),
            CopyReg(r0, r1),
            Sum(r0, r1),
            Multiply(r0, r1),
            Sub(r0, r1),
            SubM(r0, mem),
            MultiplyM(r0, mem),
            SumM(r0, mem),
            Square(r0),
            Cube(r0),
            JumpZN(r0, rjmp),
            JumpP(r0, rjmp),
            Sin(r0),
            Log(r0),
            Cos(r0),
            Exp(r0),
            Sqrt(r0),
            LoadC(r0, c),
        ];
        let mut selections: Vec<Instruction> = vec![];
        for instruction in instructions.iter() {
            if self
                .allowed_instructions
                .contains(&discriminant(instruction))
            {
                selections.push(*instruction);
            }
        }
        selections[rng.gen_range(0..selections.len())]
    }
}

pub enum CodeBlock {
    Normal(Vec<Instruction>),
    Conditional(Instruction, Vec<Instruction>),
}

pub fn flatten_block(blocks: &[CodeBlock]) -> Vec<Instruction> {
    use CodeBlock::*;
    let cap = {
        let mut c = 0usize;
        for block in blocks.iter() {
            c += match block {
                Normal(v) => v.len(),
                Conditional(_i, v) => v.len() + 1,
            };
        }
        c
    };
    let mut stack: Vec<Instruction> = Vec::with_capacity(cap);
    for block in blocks.iter() {
        match block {
            Normal(v) => {
                stack.append(&mut v.clone());
            }
            Conditional(i, v) => {
                stack.push(*i);
                stack.append(&mut v.clone());
            }
        }
    }
    stack
}

pub fn unflatten_block(stack: &[Instruction]) -> Vec<CodeBlock> {
    use CodeBlock::*;
    use Instruction::*;
    let mut blocks: Vec<CodeBlock> = vec![];
    let mut current_block: Option<usize> = None;
    for i in stack.iter() {
        match current_block {
            None => {
                let new_block = match *i {
                    JumpP(_reg, _dist) | JumpZN(_reg, _dist) => Conditional(*i, vec![]),
                    _ => Normal(vec![*i]),
                };
                current_block.replace(0);
                blocks.push(new_block);
            }
            Some(idx) => match *i {
                JumpP(_reg, _dist) | JumpZN(_reg, _dist) => {
                    let new_block = Conditional(*i, vec![]);
                    blocks.push(new_block);
                    current_block.replace(idx + 1);
                }
                _ => {
                    let v = match &mut blocks[idx] {
                        Conditional(_i, v) => v,
                        Normal(v) => v,
                    };
                    v.push(*i);
                }
            },
        };
    }
    blocks
}

pub fn clean_introns(blocks: &mut [CodeBlock]) {
    let mut targets = [false; MAXIMUM_REGISTERS];
    targets[OUTPUT_REGISTER] = true;
    use crate::instruction::Instruction::*;
    use CodeBlock::*;
    for block in blocks.iter_mut().rev() {
        let v = match block {
            Normal(v) => v,
            Conditional(_i, v) => v,
        };
        let mut ptr = (v.len() - 1) as isize;
        while ptr >= 0 {
            let mut dest: Option<usize> = None;
            match v[ptr as usize] {
                Multiply(dest_, _)
                | Sub(dest_, _)
                | Sum(dest_, _)
                | Square(dest_)
                | Sin(dest_)
                | Cos(dest_)
                | Cube(dest_)
                | CopyReg(dest_, _)
                | Log(dest_)
                | Exp(dest_)
                | Sqrt(dest_) => {
                    dest.replace(dest_);
                }
                MultiplyM(dest_, _)
                | SubM(dest_, _)
                | SumM(dest_, _)
                | Load(dest_, _)
                | LoadC(dest_, _) => {
                    dest.replace(dest_);
                }
                _ => {}
            }
            if dest.is_none() {
                continue;
            }
            if targets[dest.unwrap()] {
                match v[ptr as usize] {
                    Sub(_dest, src)
                    | Sum(_dest, src)
                    | Multiply(_dest, src)
                    | CopyReg(_dest, src) => {
                        targets[src] = true;
                    }
                    _ => {}
                }
            } else {
                v.remove(ptr as usize);
            }
            ptr -= 1;
        }
    }
}
