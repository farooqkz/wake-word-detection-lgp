import os
import math
from pprint import pprint


ft_nojump = []
ft_withjump = []

for file in os.listdir("./experiments"):
    with open("experiments/" + file) as fp:
        for line in fp.readlines():
            if "Train" in line:
                segments = line.split()[3].split("/")
                fitness = math.sqrt(float(segments[0]) * float(segments[1]))
                if "nojump" in file:
                    ft_nojump.append(fitness)
                else:
                    ft_withjump.append(fitness)


mean_nojump = sum(ft_nojump) / len(ft_nojump)
mean_withjump = sum(ft_withjump) / len(ft_withjump)
variance_nojump = sum(map(lambda x: (x-mean_nojump)**2, ft_nojump)) / len(ft_nojump)
variance_withjump = sum(map(lambda x: (x-mean_withjump)**2, ft_withjump)) / len(ft_nojump)

print("with jump", mean_withjump, variance_withjump)
print("no jump", mean_nojump, variance_nojump)
